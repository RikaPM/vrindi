<div class="col-sm-8 blog-main-posts" style="background:#D3D3D3">
          
    <!-- Post Item -->
    <div class="wow fadeIn pb-90">
        
      
        <div class="post-prev-img">
            <?php if($image = $page->images()->sortBy('sort', 'asc')->first()): ?>
                <a href="<?php echo $page->url() ?>">
                    <img src="<?php echo $image->url() ?>" alt="img">
                </a>
            <?php endif; ?>
        </div>
    

        <div class="post-prev-title">
            <h3 class="post-title-big">
                <a href="<?php echo $page->url() ?>">
                    <?php echo $page->title() ?>
                </a>
            </h3>
        </div>

        <div class="post-prev-info ">
            <?php echo date('d M Y', $page->date()) ?>
            <!-- <span class="slash-divider">/</span> -->
            <!-- <a href="">Administrator</a> -->
            <!-- <span class="slash-divider">/</span> -->
            <!-- <a href="#">Design</a>,  -->
            <!-- <a href="#">Trends</a> -->
        </div>

        <div class="mb-30">
            <?php echo $page->text()->kirbytext() ?>
        </div>
        
    
        
        <!-- Comment count, love, and share widget should goes here -->
        <?php //echo snippet('widget/mainbar/blog/share') ?>   

        <!-- Post author should goes here -->
        
        <?php include_once('blog-detail-pagination.php') ?>   
        <br>    
        <?php //echo snippet('widget/mainbar/blog/related-post') ?>   
                
        <?php echo snippet('widget/mainbar/blog/disqus-comment') ?>   
                
                
              
    </div>
</div>

