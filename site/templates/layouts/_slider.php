<?php $back1 = $site->contentURL() . "/" . $site->Slide1(); ?>
<?php $back2 = $site->contentURL() . "/" . $site->Slide2(); ?>
<?php $back3 = $site->contentURL() . "/" . $site->Slide3(); ?>
<?php $back4 = $site->contentURL() . "/" . $site->Slide4(); ?>

<div id="slider" data-section="home">
        <div class="owl-carousel owl-carousel-fullwidth">
            <!-- You may change the background color here. -->
            <div class="item" style="background-image: url(<?php echo $back1 ?>)" >
                <div class="container" style="position: relative;">
                    <div class="row">

                        <div class="col-md-7 col-sm-7">
                            <div class="fh5co-owl-text-wrap">
                                <div class="fh5co-owl-text">
                                    <h1 class="fh5co-lead to-animate">Virtual Reality</h1>
                                    <h2 class="fh5co-sub-lead to-animate">A wonderfull sight </h3>
                                    <p class="to-animate-2"><a href="#" class="btn btn-primary btn-lg">View Case Study</a></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-md-push-1 col-sm-4 col-sm-push-1 iphone-image"></div>

                    </div>
                </div>
            </div>
            <!-- You may change the background color here.  -->
            <div class="item"  style="background-image: url(<?php echo $back2 ?>)">
                <div class="overlay"></div>
                <div class="container" style="position: relative;">
                    <div class="row">
                       
                        <div class="col-md-6 col-md-push-1 col-md-push-5 col-sm-7 col-sm-push-1 col-sm-push-5">
                            <div class="fh5co-owl-text-wrap">
                                <div class="fh5co-owl-text">
                                    <h1 class="fh5co-lead to-animate">Virtual Reality</h1>
                                    <h2 class="fh5co-sub-lead to-animate">A wonderfull sight </h3>
                                    <p class="to-animate-2"><a href="#" class="btn btn-primary btn-lg">View Case Study</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

             <div class="item" style="background-image: url(<?php echo $back3 ?>">
                <div class="overlay"></div>
                <div class="container" style="position: relative;">
                    <div class="row">
                        <div class="col-md-5 col-sm-5">
                            <div class="fh5co-owl-text-wrap">
                                <div class="fh5co-owl-text">
                                    <h1 class="fh5co-lead to-animate">Virtual Experience</h1>
                                    <h2 class="fh5co-sub-lead to-animate">100% Free Tax </h3>
                                    <p class="to-animate-2"><a href="#" class="btn btn-primary btn-lg">View Case Study</a></p>
                                </div>
                            </div>
                        </div>
                        <!--<div class="col-md-4 col-md-push-1 col-sm-4 col-sm-push-1 iphone-image">
                            <div class="iphone to-animate-2"><img src="<?php// echo $back3?>" alt="Free HTML5 Template by FREEHTML5.co"></div>
                        </div>-->
                    </div>
                </div>
            </div>
            <div class="item" style="background-image: url(<?php echo $back4 ?>)">
                <div class="overlay"></div>
                <div class="container" style="position: relative;">
                    <div class="row">
                        <div class="col-md-7 col-sm-7">
                            <div class="fh5co-owl-text-wrap">
                                <div class="fh5co-owl-text">
                                    <h1 class="fh5co-lead to-animate">Virtual Reality</h1>
                                    <h2 class="fh5co-sub-lead to-animate">A wonderfull sight </h3>
                                    <p class="to-animate-2"><a href="#" class="btn btn-primary btn-lg">View Case Study</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

    </div>
</div>
                    