 <nav class="navbar navbar-default">
                <div class="navbar-header">
                    <!-- Mobile Toggle Menu Button -->
                    <a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"><i></i></a>
                    <a class="navbar-brand" href="#">VR INDI</a> 
                </div>

                <div id="navbar" class="navbar-collapse collapse">
                  <ul class="nav navbar-nav navbar-right">
                    <?php $current_page_id = $page->id() ?>
                        <?php foreach($pages->visible() as $navitem): ?>
                            <?php if($current_page_id == $navitem->id()) : ?>
                                <?php $is_active = true; ?>
                            <?php else: ?>
                                <?php $is_active = false; ?>
                            <?php endif; ?>
                        <li <?php if($is_active == true) echo "current" ?>>
                            <a href="<?php echo $navitem->url() ?>" class="<?php if($is_active == true) echo "current" ?>">
                                <div class="main-menu-title"><?php echo $navitem->title()->html() ?></div>
                            </a>
                            
                        </li>
                        <?php endforeach; ?>
                  </ul>
                </div>