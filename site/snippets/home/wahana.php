

<div id="fh5co-our-services" data-section="product" style="background:#999999">
        <div class="container">
            <div class="row row-bottom-padded-sm">
                <div class="col-md-12 section-heading text-center">
                    <h2 class="to-animate">Eksplor Wahana</h2>
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2 to-animate">
                            <h3>VR is an immersive experience in which your head movements are tracked in a three-dimensional world, making it ideally suited to games and even movies.</h3>
                        </div>
                    </div>
                </div>
            </div>
            <div class="feature-grids">
                    <?php foreach(page('wahana')->children()->limit(3) as $wahana): ?>
                    <?php $featured_image = $wahana->contentURL() . "/" . $wahana->icon() ?>
                    <div class="col-md-4 feature-grid">
                        <img src="<?php echo $featured_image?>" class="img-responsive img-rounded" alt="Free HTML5 Template">
                        <h4> <?php echo $wahana->title()?>  </h4>
                        <p>  <?php echo $wahana->text()?>  </p>
                        <a class="btn btn-primary btn-sm" href="<?php echo $wahana->url() ?>">read more</a>
                    </div>
                     <?php endforeach ?>

                    <div class="clearfix"></div>
            </div>
        
        </div>
    </div>
   