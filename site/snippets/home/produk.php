
	<div id="fh5co-our-services" data-section="product" style="background:#666666">
		<div class="container">
			<div class="row row-bottom-padded-sm">
				<div class="col-md-12 section-heading text-center">
					<h2 class="to-animate">Our Product</h2>
					<div class="row">
						<div class="col-md-8 col-md-offset-2 to-animate">
							<h3>VR is an immersive experience in which your head movements are tracked in a three-dimensional world, making it ideally suited to games and even movies.</h3>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				 <?php foreach(page('product')->children()->limit(3) as $produk): ?>
                     <?php $featured_image = $produk->contentURL() . "/" . $produk->icon() ?>
				<div class="col-md-4" class="img-responsive img-rounded" alt="Free HTML5 Template">
					<div class="box to-animate">
					<a href="<?php echo $produk->url() ?>"><img src="<?php echo $featured_image ?>"></a>
					</div>

				</div>
				
				<?php endforeach ?>
			</div>
			<div class="row">
		  <div class="related-posts">
                        
                        <div class="related-post">
                           <?php echo snippet('widget/sidebar/recent-post2', array('type' => 'produk')) ?>  
                            <div class="clearfix"> </div>
                        </div>
                       
                       
                       
                    </div>
                    <div class="row">

		</div>
	</div>