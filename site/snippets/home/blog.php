
<div id="fh5co-press" data-section="blog" style="background:#B0C4DE">
        <div class="container">
            <div class="row">
                <div class="col-md-12 section-heading text-center">
                    <h2 class="single-animate animate-press-1">Recent Blog Post</h2>
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2 subtext single-animate animate-press-2">
                            <h3>Apa yang Anda lihat saat menonton film 3D dengan kacamata khusus tersebut?</h3>
                        </div>
                    </div>
                </div>
            </div>

            <!--   template yg echo -->
    <div class="banner-bdy blg">
    <div class="blog">
        <div class="blog-grids">
            <?php foreach(page('blog')->children()->limit(4) as $blog): ?>
                     <?php $featured_image = $blog->contentURL() . "/" . $blog->icon() ?>
            <div class="col-md-3 blog-grid">
                <div class="blog-grid1">
                    <a href="blog1.php"><img src="<?php echo $featured_image ?>" alt=" " /></a>
                    <div class="blog-grid1-info">
                        <div class="soluta">
                            <a href="blog1.php"><?php echo $blog->title() ?></a> 
                                <span><?php echo date('d M Y', $blog->date()) ?></span>
                        </div>
                        <p><?php echo $blog->text() ?></p>
                        <div class="red-mre">
                            <a href="<?php echo $blog->url() ?>">Read More</a> 
                        </div>
                    </div>
                </div>
            </div>
<?php endforeach ?>


        
            <div class="clearfix"> </div>
        </div>
    </div>
    </div>
    <h4> <a href="<?php echo $site->url() ?>/blog/">View More Blogs</a></h4>
    </div>
<!-- //blog -->
            
        </div>
    </div>
