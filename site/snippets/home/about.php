    <?php foreach(page('about')->children()->limit(1) as $about): ?>
                     <?php $featured_image = $about->contentURL() . "/" . $about->icon() ?>
    <div id="fh5co-about-us" data-section="about"  style="background:#999999">
        <div class="container">
            <div class="row row-bottom-padded-lg" id="about-us">
                <div class="col-md-12 section-heading text-center">
                    <h2 class="to-animate">About Us</h2>
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2 to-animate">
                            <h3><?php echo $about->leading()?></h3>
                        </div>
                    </div>
                </div>
                <div class="col-md-8 to-animate">
                    <img src="<?php echo $featured_image ?>" class="img-responsive img-rounded" alt="Free HTML5 Template">
                </div>
                <div class="col-md-4 to-animate">
                    <h2><?php echo $about->title()?></h2>
                    <p align="justify"><?php echo $about->text()?></p>
                    <p><a href="<?php echo $about->url() ?>" class="btn btn-primary">Meet the team</a></p>
                </div>
            </div>
            <?php endforeach ?>
        </div>
    </div>
           