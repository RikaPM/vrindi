<div class="widget">
   
    <div class="widget-body mt-25">
        <ul class="clearlist widget-posts">


              <?php if($type == 'blog') : ?>
             <h3  style="margin-top:10px "  ><?php echo ucfirst($type) ?> Terbaru</h3>
            <?php foreach(page('blog')->children()->limit(6)->visible() as $produk): ?>  
                    <div class="widget-posts-descr2">
                        <a href="<?php echo $produk->url() ?>" class="lh-18" >
                            <h4><?php echo $produk->title() ?></h4>
                        </a>
                        <div class="font-poppins lh-18" ><h5><?php echo date('d M Y', $produk->date()) ?></h5></div> 
                    </div>
            <?php endforeach; ?>


            <?php elseif($type == 'wahana') : ?>
             <h3  style="margin-top:10px " ><?php echo ucfirst($type) ?> Terbaru</h3>

<?php foreach(page('wahana')->children()->limit(6)->visible() as $produk): ?>  
                    <div class="widget-posts-descr2">
                        <a href="<?php echo $produk->url() ?>" class="lh-18" >
                            <h4><?php echo $produk->title() ?><h4>
                        </a>
                        <div class="font-poppins lh-18"><h5><?php echo date('d M Y', $produk->date()) ?></h5></div> 
                    </div>
            <?php endforeach; ?>

 <?php elseif($type == 'produk') : ?>
  <h3  style="margin-top:10px " ><?php echo ucfirst($type) ?> Terbaru</h3>

<?php foreach(page('product')->children()->limit(6)->visible() as $produk): ?>  
                    <div class="widget-posts-descr2">
                        <a href="<?php echo $produk->url() ?>" class="lh-18" >
                           <h4> <?php echo $produk->title() ?></h4>
                        </a>

                        <div class="font-poppins lh-18"><h5><?php echo date('d M Y', $produk->date()) ?></h5></div> 
                    </div>
            <?php endforeach; ?>
  <?php endif; ?>

</ul>
    </div>
</div>












        </ul>
    </div>
</div>

