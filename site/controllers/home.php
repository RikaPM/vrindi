<?php
return function($site, $pages, $page) {

    $form = uniform(
        'webhook-form',
        array(
            'required'  => array(
                'name'      => '',
                'message'   => '',
                ),
            'actions' => array(
                array(
                    '_action' => 'webhook',
                    'only' => array(
                        'name'
                    ),
                    'url' => 'http://example.com/myhook',
                    'params' => array(
                        'method' => 'post',
                        'data' => array(
                            'token' => 'my_access_token'
                        )
                    )
                )
            )
        )
    );

   return compact('form');
};