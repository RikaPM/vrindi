﻿Title: Blog
----

Icon: building

----

Text: 

Dasar pembentukan struktur organisasi Dinas Perindustrian dan Perdagangan Kota Malang adalah sebagai berikut:
Peraturan Daerah Kota Malang Nomor 6 Tahun 2012 tentang Organisasi dan Tata Kerja Dinas Daerah (Lembaran Daerah Kota Malang Tahun 2012 Nomor 6, Tambahan Lembaran Daerah Kota Malang Nomor 4).
Peraturan Walikota Malang Nomor 51 Tahun 2012 Tentang Uraian Tugas Pokok, Fungsi dan Tata Kerja Dinas Perindustrian dan Perdagangan Kota Malang
Adapun susunan organisasi Dinas Perindustrian dan Perdagangan Kota Malang, terdiri dari:
Kepala Dinas;
Sekretariat terdiri dari:
Subbagian Keuangan;
Subbagian Umum;
Subbagian Penyusunan Program;
Bidang Perindustrian Agro dan Kimia , terdiri dari:
Seksi Pembinaan dan Pengembangan Industri Makanan Minuman dan Tembakau ;
Seksi Pembinaan dan Pengembangan Industri Pertanian dan Kehutanan;
Seksi Pembinaan dan Pengembangan Industri Kimia.
Bidang Perindustrian ILMETA dan IATT, terdiri dari :
Seksi Pembinaan dan Pengembangan Industri logam dan Mesin;
Seksi Pembinaan dan Pengembangan Industri Tekstil dan Aneka;
Seksi Pembinaan dan Pengembangan Industri Elektronika dan IATT (Industri Alat Transportasi dan Telematika).
Bidang Perdagangan, terdiri dari :
Seksi Bina Usaha dan Pendaftaran Perusahaan ;
Seksi Distributor dan ekspor – Impor ;
Seksi promosi
Bidang Perlindungan Konsumen, terdiri dari:
Seksi Pemberdayaan Konsumen;
Seksi Pengawasan barang Beredar dan Jasa;
Seksi Kemetrologian.
Unit Pelaksana Teknis (UPT)
Kelompok Jabatan Fungsional.
Dalam hal pelaksanaan tugas dan fungsinya, untuk kesekretariatan dipimpin oleh Sekretaris sedangkan untuk Bidang dipimpin oleh Kepala Bidang dimana masing-masing berada di bawah dan bertanggungjawab kepada Kepala Dinas.