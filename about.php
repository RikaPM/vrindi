<?php include_once( 'head.php' ) ?>
<?php include_once( 'nav.php' ) ?>
<div id="fh5co-about-us" data-section="about" style="background:#D3D3D3">
		<div class="container">
			<div class="row row-bottom-padded-lg" id="about-us">
				<div class="col-md-12 section-heading text-center">
					<h2 class="to-animate">About Us</h2>
					<div class="row">
						<div class="col-md-8 col-md-offset-2 to-animate">
							<h3>Perusahaan Indi menyediakan wahana virtual reality untuk anda</h3>
						</div>
					</div>
				</div>
				<div class="col-md-8 to-animate">
					<img src="images2/vr2.jpg" class="img-responsive img-rounded" alt="Free HTML5 Template">
				</div>
				<div class="col-md-4 to-animate">
					<h2>How we got started</h2>
					<p>Bagi sebagian besar orang awam tentu bertanya-tanya, apa itu virtual reality? Virtual reality bukanlah sebuah helm atau masker yang digunakan Darth Vader di film Star Wars, ini merupakan sebuah teknologi yang memungkinkan Anda untuk berinteraksi dengan objek imajinasi dengan menggunakan komputer dan membawa Anda ke dalam suasana 3-Dimensi yang seolah nyata. </p>
					<p>Anda mungkin pernah mencoba untuk menonton film 3-Dimensi dengan bantuan kacamata khusus, bukan? Apa yang Anda lihat saat menonton film 3D dengan kacamata khusus tersebut? </p>
					<p><a href="about1.php" class="btn btn-primary">Meet the team</a></p>
				</div>
			</div>
		</div>
	</div>